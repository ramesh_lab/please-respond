package com.enlighten.rsvp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.enlighten.rsvp.service.StreamReader;

@SpringBootApplication
public class RsvpApplication {
    @Autowired
    private StreamReader streamReader;

    public static void main(String[] args) {
        SpringApplication.run(RsvpApplication.class, args);
    }

}
