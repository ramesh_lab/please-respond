package com.enlighten.rsvp.controller;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.enlighten.rsvp.service.StreamReader;

@RestController
@RequestMapping("/rsvp")
public class RSVPController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RSVPController.class);
    @Autowired
    private StreamReader streamReader;

    @RequestMapping("/aggregate")
    public ResponseEntity<String> aggregate(@Min(1) @Max(Long.MAX_VALUE) @RequestParam(name = "duration", defaultValue = "60") Long duration,
                                            @RequestParam(name = "timeUnit", defaultValue = "SECONDS") TimeUnit timeUnit) {
        try {
            return ResponseEntity.ok(streamReader.readResponse(duration, timeUnit));
        } catch (IOException e) {
            LOGGER.error("Error ", e);
        } catch (InterruptedException e) {
            LOGGER.error("Error ", e);
        } catch (ExecutionException e) {
            LOGGER.error("Error ", e);
        }
        return ResponseEntity.internalServerError().build();
    }

}
