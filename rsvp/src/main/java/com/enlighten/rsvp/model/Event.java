
package com.enlighten.rsvp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;
import java.text.SimpleDateFormat;
import java.util.Date;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Event {

    private SimpleDateFormat format = new SimpleDateFormat("YYYY-mm-dd HH:MM");
    @JsonProperty("event_id")
    private String eventId;
    @JsonProperty("event_name")
    private String eventName;
    @JsonProperty("event_url")
    private String eventUrl;
    @JsonProperty
    private Long time;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventUrl() {
        return eventUrl;
    }

    public void setEventUrl(String eventUrl) {
        this.eventUrl = eventUrl;
    }

    public Long getTime() {
        return time;
    }

    public String getFormatedDate() {
        return format.format(new Date(time));
    }

    public void setTime(Long time) {
        this.time = time;
    }

}
