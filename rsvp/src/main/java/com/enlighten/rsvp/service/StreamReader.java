package com.enlighten.rsvp.service;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.enlighten.rsvp.model.Event;
import com.enlighten.rsvp.model.RSVP;

@Service
public class StreamReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(StreamReader.class);

    @Autowired
    RestTemplate restTemplate;

    public String readResponse(Long duration, TimeUnit timeUnit) throws IOException, InterruptedException, ExecutionException {
        Map<String, Integer> map = new HashMap<>();
        Set<Event> eventSet = new TreeSet<>(Comparator.comparing(Event::getTime).reversed());
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<Integer> integerFuture = executorService.submit(() -> {
            int count = 0;
            long start = System.currentTimeMillis();
            do {
                RSVP rsvp = restTemplate.getForObject("https://stream.meetup.com/2/rsvps", RSVP.class);
                count++;
                eventSet.add(rsvp.getEvent());
                map.computeIfPresent(rsvp.getGroup().getGroupCountry(), (k, v) -> v + 1);
                map.computeIfAbsent(rsvp.getGroup().getGroupCountry(), k -> 1);
            } while (System.currentTimeMillis() - start < timeUnit.toMillis(duration));
            LOGGER.info("Time taken to read URL = {}", System.currentTimeMillis() - start);
            return count;
        });
        executorService.awaitTermination(duration, timeUnit);
        executorService.shutdown();
        LOGGER.info("Size of map={}", map.size());
        LOGGER.info("Number of RSVP={}", integerFuture.get()); // wait for response from thread
        Map<String, Integer> sortedMap = map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        Event furthest = eventSet.iterator().next();
        StringBuilder sb = new StringBuilder("");
        sb.append(integerFuture.get()).append(",").append(furthest.getFormatedDate()).append(",").append(furthest.getEventUrl()).append(",");
        int count = 0;
        for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
            count++;
            sb.append(entry.getKey()).append(",").append(entry.getValue()).append(",");
            if (count >= 3)
                break;
        }
        return sb.deleteCharAt(sb.length() - 1).toString();
    }

}
